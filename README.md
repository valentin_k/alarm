# README #

### Что ###

* Будильник на вебсокетах и RabbitMQ https://github.com/rabbitmq/rabbitmq-delayed-message-exchange

### Как с этим жить ###

* Чтобы просто запустить:
  
    ```docker-compose up -d```


### Как это работает ###

* Свагер <http://127.0.0.1:8000/docs#>
* <http://127.0.0.1:8000/alarm_feed> - смотреть сообщения будильника


* /create_alarm/ - создать будильник. Если отправить null в качестве задержки, поставит на 5 секунд в будущее (см. schemas.py)
    
```curl -X POST "http://127.0.0.1:8000/create_alarm/" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"delay\":null,\"text\":\"string\"}"```

```javascript
{
    "delay":"5,
    "text":"string",
}
```
