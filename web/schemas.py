from typing import Optional
from pydantic import PositiveFloat

import settings
from pydantic import UUID4, BaseModel, validator


class AlarmSchema(BaseModel):
    delay: Optional[PositiveFloat]
    text: str

    @validator('delay', pre=True, always=True)
    def default_delay(cls, t):
        return t or settings.DEFAULT_DELAY

