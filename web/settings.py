import os


DEFAULT_DELAY = int(os.environ.get('DEFAULT_DELAY', '5'))

AMQP_CREDS = {
    'login': 'guest',
    'password': 'guest',
    'host': 'rabbit',
}