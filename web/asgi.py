import asyncio
import json
from html import ws as ws_front

from aio_pika import connect_robust, Message, DeliveryMode

import settings
from fastapi import FastAPI, WebSocket
from fastapi.responses import HTMLResponse
from schemas import AlarmSchema

app = FastAPI()


@app.on_event('startup')
async def startup_event():
    app.amqp_connection = await connect_robust(
        'amqp://{login}:{password}@{host}/'.format(**settings.AMQP_CREDS),
        loop=asyncio.get_running_loop()
    )
    app.amqp_channel = await app.amqp_connection.channel()

    app.alarm_exchange = await app.amqp_channel.declare_exchange(
        'alarm',
        'x-delayed-message',
        durable=False,
        arguments={'x-delayed-type': 'direct'}
    )

    app.amqp_queue = await app.amqp_channel.declare_queue(
        'alarm', durable=True
    )

    await app.amqp_queue.bind(
        app.alarm_exchange,
        routing_key='alarm'
    )


@app.on_event('shutdown')
async def shutdown_event():
    await app.amqp_channel.close()
    await app.amqp_connection.close()


@app.post('/create_alarm/')
async def create_item(
    alarm: AlarmSchema
):
    message = Message(
        json.dumps(
            {'message': alarm.text}
        ).encode(),
        delivery_mode=DeliveryMode.PERSISTENT,
        headers={'x-delay': int(alarm.delay * 1000)}
    )

    await app.alarm_exchange.publish(
        message,
        routing_key='alarm'
    )

    return alarm


@app.get("/alarm_feed")
async def alarm_feed():
    return HTMLResponse(ws_front)


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    
    async with app.amqp_queue.iterator() as queue_iter:
        async for message in queue_iter:
            async with message.process():
                decoded = json.loads(message.body.decode())

                await websocket.send_text(decoded['message'])
